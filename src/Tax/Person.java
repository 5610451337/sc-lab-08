package Tax;

public class Person implements Taxable {
	private String name;
	private double revenue;
	private double tax;
	
	public String getName(){
		return name;
	}
	public Person(String aName,double aRevenue){
		name=aName;
		revenue=aRevenue;
	}
	
	@Override
	public double getTax() {
		if(revenue<300000){
			tax=revenue*0.05;
		}
		else{
			tax=((revenue-300000)*0.1)+15000;
		}
		return tax;
	}

}
