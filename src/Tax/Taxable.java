package Tax;

public interface Taxable {
	double getTax();
	String getName();

}
