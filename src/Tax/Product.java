package Tax;

public class Product implements Taxable  {
	private String name;
	private double price;
	private double tax;
	
	public Product(String aname,double aprice){
		name = aname;
		price = aprice;
	}
	@Override
	public double getTax() {
		tax=price*0.07;
		return tax;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

}
