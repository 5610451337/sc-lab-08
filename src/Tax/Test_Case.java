package Tax;

import java.util.ArrayList;

public class Test_Case {
	public static void main(String[] args){
		Person person1 = new Person("Pond",100000);
		Person person2 = new Person("Wut",500000);
		Person person3 = new Person("Keen",700000);
		ArrayList<Taxable> taxpreson = new ArrayList<Taxable>();
		taxpreson.add(person1);
		taxpreson.add(person2);
		taxpreson.add(person3);
		System.out.println("Person :: "+TaxCalculator.sum(taxpreson));
		
		Company Company1 = new Company("FaceBook",100000,50000);
		Company Company2 = new Company("HOnda",500000,6000000);
		Company Company3 = new Company("KU",700000,70000);
		ArrayList<Taxable> taxCompany = new ArrayList<Taxable>();
		taxCompany.add(Company1);
		taxCompany.add(Company2);
		taxCompany.add(Company3);
		System.out.println("Company :: "+TaxCalculator.sum(taxCompany));
		
		
		Product Product1 = new Product("ley",100000);
		Product Product2 = new Product("testo",500000);
		Product Product3 = new Product("Pen",700000);
		ArrayList<Taxable> taxProduct = new ArrayList<Taxable>();
		taxProduct.add(Product1);
		taxProduct.add(Product2);
		taxProduct.add(Product3);
		System.out.println("Product :: "+TaxCalculator.sum(taxProduct));
		
		
		ArrayList<Taxable> Alltax = new ArrayList<Taxable>();
		Alltax.add(person1);
		Alltax.add(Company1);
		Alltax.add(Product1);
		System.out.println("All :: "+TaxCalculator.sum(Alltax));
		
		
		
	}

}
