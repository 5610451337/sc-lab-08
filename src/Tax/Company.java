package Tax;

public class Company implements Taxable {
	private String name;
	private double revenue;
	private double expenditure;
	private double tax;
	
	public Company(String aName,double aRevenue,double aexpenditure){
		name=aName;
		revenue=aRevenue;
		expenditure=aexpenditure;
	}
	
	@Override
	public double getTax() {
		if(revenue>expenditure){
			tax=(revenue-expenditure)*0.3;
		}
		else{
			tax=0;
		}
		return tax;
	}

	@Override
	public String getName() {
		return name;
	}
	

}
