package Method_min;

public class BankAccount implements Measurable {
	private String name;
	private Double balance;


	public BankAccount(String aname,double abalance){
		name=aname;
		balance=abalance;
	}
	public String getName(){
		return name;
	}
	public double getBalance(){
		return balance;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return balance;
	}
	

}
