package Method_min;

public interface Measurable {
	double getMeasure(); 
	
	String getName();

}
