package Method_min;

public class Test_Case {
	public static void main(String[] args){
		
		Measurable[] Person = new Measurable[3];
		Person[0]=new Person("Pond",183);
		Person[1]=new Person("Kik",162);
		Person[2]=new Person("Wut",172);
		Measurable lower = Data.min(Person[0],Person[1] );
		System.out.println("lower Person  " + lower.getName());
		System.out.println("------------------------");
		
		
		
		
		Measurable[] accounts = new Measurable[3];
		accounts[0] = new BankAccount("Pond",1000);
		accounts[1] = new BankAccount("Kik",10000);
		Measurable lesser = Data.min(accounts[0],accounts[1] );
		System.out.println("lesser Acount  " + lesser.getName());
		System.out.println("------------------------");
		
		
		
		Measurable[] Country = new Measurable[3];
		Country[0] = new Country("Uraguay", 176220);
		Country[1] = new Country("Thailand", 514000);
		Measurable less = Data.min(Country[0], Country[1]);
		System.out.println("minor Country  " + less.getName());
		System.out.println("------------------------");
		
	}

}
